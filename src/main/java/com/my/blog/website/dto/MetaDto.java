package com.my.blog.website.dto;

import com.my.blog.website.modal.Vo.MetaVo;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
public class MetaDto extends MetaVo {

    private int count;
}
