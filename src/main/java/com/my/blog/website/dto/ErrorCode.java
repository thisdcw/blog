package com.my.blog.website.dto;

/**
 * 错误提示
 * Created by thisdcw on 2023/6/26.
 */
public interface ErrorCode {
    /**
     * 非法请求
     */
    String BAD_REQUEST = "BAD REQUEST";

}
