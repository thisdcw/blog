package com.my.blog.website.modal.Bo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * Created by thisdcw on 2023/6/25.
 */
@Getter
@Setter
public class BackResponseBo implements Serializable {

    private String attachPath;
    private String themePath;
    private String sqlPath;

}
