package com.my.blog.website.modal.Vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author thisdcw
 */
@Getter
@Setter
public class OptionVo implements Serializable {
    /**
     * 配置名称
     */
    private String name;

    /**
     * 配置值
     */
    private String value;

    private String description;

    private static final long serialVersionUID = 1L;

}