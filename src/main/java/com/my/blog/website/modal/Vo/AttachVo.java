package com.my.blog.website.modal.Vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author thisdcw
 */
@Getter
@Setter
public class AttachVo implements Serializable {
    private Integer id;

    private String fname;

    private String ftype;

    private String fkey;

    private Integer authorId;

    private Integer created;

    private static final long serialVersionUID = 1L;

}