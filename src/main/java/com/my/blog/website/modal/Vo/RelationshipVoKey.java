package com.my.blog.website.modal.Vo;

import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

/**
 * @author thisdcw
 */
@Getter
@Setter
public class RelationshipVoKey implements Serializable {
    /**
     * 内容主键
     */
    private Integer cid;

    /**
     * 项目主键
     */
    private Integer mid;

    private static final long serialVersionUID = 1L;

}